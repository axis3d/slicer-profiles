# Slicer Profiles

Slicer profiles for all the Axis 3D printers.  Most common slicers are covered.

- Cura
- Simplify 3D
- Slic3r PE
- Pathio
- Kisslicer

Others will be added upon request.